package main

import (
	"fmt"

	"github.com/fatih/color"
)
func infoCountry(submissions []Submission, needLog bool, country Country)  {
	submissionsByCountry := map[string][]Submission{}

	for _, submission := range submissions {
		countryName := submission.Country.Name
		submissionsByCountry[countryName] = append(submissionsByCountry[submission.Country.Name], submission)
	}

	if outputFlag == "wide" {
		color.Yellow("Statistics by country:")
		for i, sByCountry := range submissionsByCountry {
			color.Yellow("\t %v %v", i, len(sByCountry))
		}
		fmt.Println()
	}

	countrySubmissions := submissionsByCountry[country.Name]

	idx := -1
	for i, submission := range countrySubmissions {
		if submission.Id == watchingSubmissionFlag {
			idx = i
			break
		}
	}

	color.Green(fmt.Sprintf("Our position at country [%s]: %v", country.Name, idx + 1))

	if outputFlag == "normal" || outputFlag == "wide" {
		firstTen := countrySubmissions[0:10]

		fmt.Println("Printing first ten submissions in Russian Federation")
		for i, submission := range firstTen {
			formattedSubmission := fmt.Sprintf("%v) %v \n", i + 1, submission.toString())

			if submission.Id == watchingSubmissionFlag {
				color.Red(formattedSubmission)
			} else {
				color.White(formattedSubmission)
			}
		}

		if needLog {
			logger.Print("\n[COUNTRY] --- Printing first 20 submissions ---\n")
			for i, submission := range countrySubmissions[0:20] {
				formattedSubmission := fmt.Sprintf("%v) %v", i + 1, submission.toString())

				logger.Printf("%v\n", formattedSubmission)

			}
			logger.Print("[COUNTRY] --- END --- \n")
		}
	}

	fmt.Println()
}

