package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/inancgumus/screen"
	"github.com/sirupsen/logrus"
)

const submissionsUrl = "https://addons-redbullbasementuniversity2019.redbull.com/public-api/submissions/"

type Country struct {
	Name string `json:"name"`
}

type Tag struct {
	Id       int    `json:"id"`
	Name     string `json:"xivado_tag_id"`
	RealName string `json:"name"`
}

type Submission struct {
	Id        int     `json:"id"`
	Title     string  `json:"title"`
	Tags      []Tag   `json:"tags"`
	Country   Country `json:"country"`
	VoteCount int     `json:"vote_count"`
}

func (s *Submission) joinedTags() string {
	var tagNames []string
	for _, tag := range s.Tags {
		tagNames = append(tagNames, tag.Name)
	}

	return strings.Join(tagNames, ", ")
}

func (s *Submission) toString() string {
	return fmt.Sprintf("Votes: %v, Title \"%v\", Country: \"%v\", Tags: [ %v ]",
		s.VoteCount,
		s.Title,
		s.Country.Name,
		s.joinedTags(),
	)
}

type SubmissionRespJson struct {
	Submissions []Submission `json:"submissions"`
}

var outputFlag = "normal"
var intervalFlag int64 = 2
var watchFlag = false
var watchingSubmissionFlag = 423 // Bonch.dev
var logRotationFlag = time.Minute * time.Duration(30)
var logger log.Logger

func init() {
	flag.IntVar(&watchingSubmissionFlag, "s", 423, "What we are watching for")
	flag.DurationVar(&logRotationFlag, "r", time.Minute*time.Duration(30), "Rotation time of logfile")
	flag.StringVar(&outputFlag, "o", "normal", "Output format of results")
	flag.Int64Var(&intervalFlag, "i", 2, "Interval of update watch")
	flag.BoolVar(&watchFlag, "watch", false, "Starting watching")
	flag.Parse()

	availableOutputFlags := []string{"normal", "wide", "small"}

	if !contains(availableOutputFlags, outputFlag) {
		outputFlag = "normal"
	}
}

//noinspection GoUnhandledErrorResult
func main() {
	if watchFlag {
		screen.Clear()

		var logTimer = logRotationFlag

		for {
			if logTimer >= logRotationFlag {

				fileName := time.Now().Format("2006.01.02_15:04:05") + ".log"

				file, err := os.OpenFile(fileName, os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
				if err == nil {
					logger.SetOutput(file)
				} else {
					logger.Fatal("Failed to log to file")
				}

				logTimer = time.Duration(0)

				work(true)
				file.Close()
			} else {
				work(false)
			}

			diff := time.Second * time.Duration(intervalFlag)

			time.Sleep(diff)

			logTimer = logTimer + diff
		}
	}

	fileName := time.Now().Format("2006.01.02_15:04:05") + ".log"

	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
	defer file.Close()

	if err == nil {
		logger.SetOutput(file)
	} else {
		logger.Fatal("Failed to log to file")
	}

	work(true)
}

func work(needLog bool) {
	client := &http.Client{}

	req, err := http.NewRequest("GET",
		submissionsUrl,
		bytes.NewBufferString(""))
	if err != nil {
		logrus.Error(err)
	}

	req.Header.Add("Content-Type", "application/json")

	submissionResp, err := client.Do(req)
	if err != nil {
		logrus.Error(err)
	}

	submissionRespData, err := ioutil.ReadAll(submissionResp.Body)
	if err != nil {
		logrus.Error(err)
	}

	var submissionRespJson SubmissionRespJson

	if err := json.Unmarshal(submissionRespData, &submissionRespJson); err != nil {
		logrus.Error(err)
	}

	submissions := submissionRespJson.Submissions

	sort.Slice(submissions, func(i, j int) bool {
		return submissions[i].VoteCount > submissions[j].VoteCount
	})

	var ourSubmission Submission
	for _, submission := range submissions {
		if submission.Id == watchingSubmissionFlag {
			ourSubmission = submission
			break
		}
	}

	if watchFlag {
		screen.Clear()
		screen.MoveTopLeft()
		fmt.Printf("Watching with interval: %v. \t\t [ %s ]\n\n", intervalFlag, time.Now().Format(time.RFC1123))
	}

	infoTotal(submissions, needLog)
	infoCountry(submissions, needLog, ourSubmission.Country)
	infoTag(submissions, needLog, ourSubmission.Tags)
}

func contains(s []string, searchTerm string) bool {
	i := sort.SearchStrings(s, searchTerm)
	return i < len(s) && s[i] == searchTerm
}
