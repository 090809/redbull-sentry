package main

import (
	"fmt"

	"github.com/fatih/color"
)

func infoTotal(submissions []Submission, needLog bool) {

	idx := -1
	for i, submission := range submissions {
		if submission.Id == watchingSubmissionFlag {
			idx = i
			break
		}
	}

	color.Green("Our position in world: %v\n", idx + 1)

	if outputFlag == "normal" || outputFlag == "wide" {
		firstN := submissions[0:15]

		fmt.Println("Printing first fifteen submissions")
		for i, submission := range firstN {
			formattedSubmission := fmt.Sprintf("%v) %v \n", i + 1, submission.toString())

			if submission.Id == watchingSubmissionFlag {
				color.Red(formattedSubmission)
			} else {
				color.White(formattedSubmission)
			}
		}

		if needLog {
			logger.Print("\n[GLOBAL] --- Printing first 50 submissions ---\n")
			for i, submission := range submissions[0:50] {
				formattedSubmission := fmt.Sprintf("%v) %v", i + 1, submission.toString())

				logger.Printf("%v\n", formattedSubmission)

			}
			logger.Print("[GLOBAL] --- END --- \n")
		}
	}

	fmt.Println()
}

