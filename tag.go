package main

import (
	"fmt"

	"github.com/fatih/color"
)

func infoTag(submissions []Submission, needLog bool, tagNames []Tag)  {
	submissionsByTag := map[string][]Submission{}

	for _, submission := range submissions {
		tags := submission.Tags
		for _, tag := range tags {
			submissionsByTag[tag.Name] = append(submissionsByTag[tag.Name], submission)
		}
	}

	if outputFlag == "wide" {
		color.Yellow("Statistics by tag:")
		for i, sByCountry := range submissionsByTag {
			color.Yellow("\t %v %v", i, len(sByCountry))
		}
		fmt.Println()
	}

	for i := 0; i < len(tagNames); i++ {
		submissionsTagged := submissionsByTag[tagNames[i].Name]

		idx := -1
		for i, submission := range submissionsTagged {
			if submission.Id == watchingSubmissionFlag {
				idx = i
				break
			}
		}

		color.Green(fmt.Sprintf("Our position at tag [%s]: %v", tagNames[i].Name, idx + 1))

		if outputFlag == "normal" || outputFlag == "wide" {
			firstTen := submissionsTagged[0:10]

			fmt.Printf("Printing first ten submissions in tag [%s] \n", tagNames[i].Name)
			for i, submission := range firstTen {
				formattedSubmission := fmt.Sprintf("%v) %v \n", i + 1, submission.toString())

				if submission.Id == watchingSubmissionFlag {
					color.Red(formattedSubmission)
				} else {
					color.White(formattedSubmission)
				}
			}

			if needLog {
				logger.Print("\n[TAG] --- Printing first 20 submissions ---\n")
				for i, submission := range submissionsTagged[0:20] {
					formattedSubmission := fmt.Sprintf("%v) %v", i + 1, submission.toString())

					logger.Printf("%v\n", formattedSubmission)

				}
				logger.Print("[TAG] --- END --- \n")
			}
		}

		fmt.Println()
	}
}
